package com.parkinglot.dto;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedPackingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {

    @Test
    void should_park_a_lot_has_more_empty_position_when_parking_given_two_parkingLot() {
        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.getParkingLots().add(new ParkingLot());

        for (int i = 0; i < 8; i++) {
            parkingBoy.getParkingLots().get(0).getTicketCarMap().put(new Ticket(i), new Car());
        }

        Car car = new Car();

        parkingBoy.park(car, "smart");

        assertEquals(1, parkingBoy.getParkingLots().get(1).getTicketCarMap().size());
        assertEquals(8, parkingBoy.getParkingLots().get(0).getTicketCarMap().size());
    }

    @Test
    void should_park_a_lot_has_more_empty_position_when_parking_given_two_parkingLot_but_different_capacity() {
        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.getParkingLots().add(new ParkingLot(15));

        for (int i = 0; i < 8; i++) {
            parkingBoy.getParkingLots().get(0).getTicketCarMap().put(new Ticket(i), new Car());
            parkingBoy.getParkingLots().get(1).getTicketCarMap().put(new Ticket(i), new Car());
        }

        Car car = new Car();

        parkingBoy.park(car, "smart");

        assertEquals(9, parkingBoy.getParkingLots().get(1).getTicketCarMap().size());
        assertEquals(8, parkingBoy.getParkingLots().get(0).getTicketCarMap().size());
    }

    @Test
    void should_throw_exception_when_parking_given_two_full_parkingLot() {
        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.getParkingLots().add(new ParkingLot());

        for (int i = 0; i < 10; i++) {
            parkingBoy.getParkingLots().get(0).getTicketCarMap().put(new Ticket(i), new Car());
            parkingBoy.getParkingLots().get(1).getTicketCarMap().put(new Ticket(i), new Car());
        }

        Car car = new Car();

        NoAvailablePositionException noAvailablePositionException = assertThrows(NoAvailablePositionException.class,
                () -> parkingBoy.park(car, "smart"));

        assertEquals("No available position.", noAvailablePositionException.getMessage());
    }

    @Test
    void should_throw_exception_when_fetch_given_wrong_ticket() {
        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.getParkingLots().add(new ParkingLot());

        for (int i = 0; i < 10; i++) {
            parkingBoy.getParkingLots().get(0).getTicketCarMap().put(new Ticket(i), new Car());
            parkingBoy.getParkingLots().get(1).getTicketCarMap().put(new Ticket(i), new Car());
        }

        Ticket ticket = new Ticket(13);

        UnrecognizedPackingTicketException unrecognizedPackingTicketException = assertThrows(UnrecognizedPackingTicketException.class, () -> parkingBoy.fetch(ticket));

        assertEquals("Unrecognized parking ticket.", unrecognizedPackingTicketException.getMessage());
    }
}
