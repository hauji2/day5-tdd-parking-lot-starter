package com.parkinglot.dto;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedPackingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_fetch_car_when_fetchCar_given_a_parking_lot_and_ticket() {
        // given
        Car car = new Car();
        Ticket ticket = new Ticket(1);

        ParkingLot parkingLot = new ParkingLot();
        parkingLot.getTicketCarMap().put(ticket, car);

        // when
        Car actual = parkingLot.fetchCar(ticket);

        assertNotNull(actual);
    }

    @Test
    void should_fetch_correct_car_when_fetchCar_given_two_parked_cars_and_two_tickets() {
        // given
        Car car = new Car();
        Car car2 = new Car();

        Ticket ticket = new Ticket(1);
        Ticket ticket2 = new Ticket(2);

        ParkingLot parkingLot = new ParkingLot();
        parkingLot.getTicketCarMap().put(ticket, car);
        parkingLot.getTicketCarMap().put(ticket2, car2);

        Car actual = parkingLot.fetchCar(ticket);

        assertEquals(car, actual);
    }

    @Test
    void should_fetch_nothing_when_fetchCar_given_wrong_ticket() {
        // given
        Car car = new Car();
        Car car2 = new Car();

        Ticket ticket = new Ticket(1);
        Ticket ticket2 = new Ticket(2);

        ParkingLot parkingLot = new ParkingLot();
        parkingLot.getTicketCarMap().put(ticket, car);
        parkingLot.getTicketCarMap().put(ticket2, car2);

        Ticket wrongTicket = new Ticket(3);

        UnrecognizedPackingTicketException exception = assertThrows(UnrecognizedPackingTicketException.class,
                () -> parkingLot.fetchCar(wrongTicket));

        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_fetch_nothing_when_fetchCar_given_used_ticket() {
        // given
        Car car = new Car();

        Ticket ticket = new Ticket(1);
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.getTicketCarMap().put(ticket, car);

        Ticket wrongTicket = new Ticket(2);

        // when
        UnrecognizedPackingTicketException exception = assertThrows(UnrecognizedPackingTicketException.class,
                () -> parkingLot.fetchCar(wrongTicket));

        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_ticket_when_parking_given_a_parking_lot_and_car() {
        // given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();


        // when
        Ticket actual = parkingLot.parkingCar(car);

        assertNotNull(actual);
    }

    @Test
    void should_return_nothing_when_parking_given_a_parking_lot_with_no_space_and_car() {
        // given
        ParkingLot parkingLot = new ParkingLot();

        for (int i = 0; i < 10; i++) {
            parkingLot.getTicketCarMap().put(new Ticket(i), new Car());
        }

        Car car = new Car();

        // when
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class,
                () -> parkingLot.parkingCar(car));

        assertEquals("No available position.", exception.getMessage());
    }
}
