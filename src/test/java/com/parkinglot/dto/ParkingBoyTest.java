package com.parkinglot.dto;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedPackingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {

    @Test
    void should_fetch_car_when_fetch_given_a_parking_lot_and_ticket_and_parkingBoy() {
        // given
        ParkingBoy parkingBoy = new ParkingBoy();

        Car car = new Car();
        Ticket ticket = new Ticket(1);

        parkingBoy.getParkingLots().get(0).getTicketCarMap().put(ticket, car);

        // when
        Car actual = parkingBoy.fetch(ticket);

        assertNotNull(actual);
    }

    @Test
    void should_fetch_correct_car_when_fetchCar_given_two_parked_cars_and_two_tickets() {

        ParkingBoy parkingBoy = new ParkingBoy();

        // given
        Car car = new Car();
        Car car2 = new Car();

        Ticket ticket = new Ticket(1);
        Ticket ticket2 = new Ticket(2);

        parkingBoy.getParkingLots().get(0).getTicketCarMap().put(ticket, car);
        parkingBoy.getParkingLots().get(0).getTicketCarMap().put(ticket2, car2);


        Car actual = parkingBoy.fetch(ticket);

        assertEquals(car, actual);
    }

    @Test
    void should_fetch_nothing_when_fetchCar_given_wrong_ticket() {
        // given
        ParkingBoy parkingBoy = new ParkingBoy();

        // given
        Car car = new Car();
        Car car2 = new Car();

        Ticket ticket = new Ticket(1);
        Ticket ticket2 = new Ticket(2);

        parkingBoy.getParkingLots().get(0).getTicketCarMap().put(ticket, car);
        parkingBoy.getParkingLots().get(0).getTicketCarMap().put(ticket2, car2);

        Ticket wrongTicket = new Ticket(3);

        UnrecognizedPackingTicketException exception = assertThrows(UnrecognizedPackingTicketException.class,
                () -> parkingBoy.fetch(wrongTicket));

        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_fetch_nothing_when_fetchCar_given_used_ticket() {
        // given
        Car car = new Car();

        Ticket ticket = new Ticket(1);

        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.getParkingLots().get(0).getTicketCarMap().put(ticket, car);

        Ticket wrongTicket = new Ticket(2);

        // when
        UnrecognizedPackingTicketException exception = assertThrows(UnrecognizedPackingTicketException.class,
                () -> parkingBoy.fetch(wrongTicket));

        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_ticket_when_parking_given_a_parking_lot_and_car() {
        // given
        ParkingBoy parkingBoy = new ParkingBoy();
        Car car = new Car();


        // when
        Ticket actual = parkingBoy.park(car, "standard");

        assertNotNull(actual);
    }

    @Test
    void should_return_nothing_when_parking_given_a_parking_lot_with_no_space_and_car() {
        // given
        ParkingBoy parkingBoy = new ParkingBoy();

        for (int i = 0; i < 10; i++) {
            parkingBoy.getParkingLots().get(0).getTicketCarMap().put(new Ticket(i), new Car());
        }

        Car car = new Car();

        // when
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class,
                () -> parkingBoy.park(car, "standard"));

        assertEquals("No available position.", exception.getMessage());
    }

    @Test
    void should_park_same_lot_when_parking_given_a_parkingLot_with_two_space_and_two_car() {
        ParkingBoy parkingBoy = new ParkingBoy();
        Car car = new Car();
        Car car2 = new Car();

        parkingBoy.park(car, "standard");
        parkingBoy.park(car2, "standard");

        assertEquals(2, parkingBoy.getParkingLots().get(0).getTicketCarMap().size());
    }

    @Test
    void should_park_another_lot_when_parking_given_one_lot_is_full_and_another_is_not() {
        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.getParkingLots().add(new ParkingLot());

        for (int i = 0; i < 10; i++) {
            parkingBoy.getParkingLots().get(0).getTicketCarMap().put(new Ticket(i), new Car());
        }

        Car car = new Car();

        parkingBoy.park(car, "standard");

        assertEquals(1, parkingBoy.getParkingLots().get(1).getTicketCarMap().size());
    }

    @Test
    void should_fetch_correct_car_when_fetch_given_two_parking_lot_and_ticket_and_parkingBoy() {
        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.getParkingLots().add(new ParkingLot());

        for (int i = 0; i < 3; i++) {
            parkingBoy.getParkingLots().get(0).getTicketCarMap().put(new Ticket(i), new Car());
        }

        parkingBoy.getParkingLots().get(1).getTicketCarMap().put(new Ticket(5), new Car());


        Ticket ticket = new Ticket(1);
        Ticket ticket2 = new Ticket(5);

        parkingBoy.fetch(ticket);
        parkingBoy.fetch(ticket2);

        assertEquals(0, parkingBoy.getParkingLots().get(1).getTicketCarMap().size());
        assertEquals(2, parkingBoy.getParkingLots().get(0).getTicketCarMap().size());
    }
}
