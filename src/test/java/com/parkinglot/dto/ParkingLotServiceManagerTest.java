package com.parkinglot.dto;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedPackingTicketException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotServiceManagerTest {
    @Test
    void should_have_parking_boy_under_parkingLotServiceManager_when_add_parking_boy_given_manager_and_parking_boy() {
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingBoy parkingBoy = new ParkingBoy();

        parkingLotServiceManager.addParkingBoy(parkingBoy);

        assertTrue(parkingLotServiceManager.getParkingBoys().contains(parkingBoy));
    }

    @Test
    void should_return_tickets_when_specifyParkingBoyToPark_given_parking_boy_and_cars() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingBoy parkingBoy = new ParkingBoy();
        ParkingBoy parkingBoy2 = new ParkingBoy();

        parkingBoy2.getParkingLots().remove(0);
        parkingBoy2.getParkingLots().add(new ParkingLot(0));

        parkingLotServiceManager.addParkingBoy(parkingBoy);
        parkingLotServiceManager.addParkingBoy(parkingBoy2);

        Car car = new Car();

        // when
        Ticket ticket = parkingLotServiceManager.specifyParkingBoyToPark(car);

        // then
        assertTrue(parkingBoy.getParkingLots().get(0).getTicketCarMap().containsKey(ticket));
    }

    @Test
    void should_throw_exception_when_specifyParkingBoyToPark_given_no_parking_boy_available() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingBoy parkingBoy = new ParkingBoy();
        ParkingBoy parkingBoy2 = new ParkingBoy();

        parkingBoy.getParkingLots().clear();
        parkingBoy.getParkingLots().add(new ParkingLot(0));
        parkingBoy2.getParkingLots().remove(0);
        parkingBoy2.getParkingLots().add(new ParkingLot(0));
        parkingLotServiceManager.addParkingBoy(parkingBoy);
        parkingLotServiceManager.addParkingBoy(parkingBoy2);

        Car car = new Car();

        // when
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class,
                () -> parkingLotServiceManager.specifyParkingBoyToPark(car));

        // then
        assertTrue(exception.getMessage().contains("No available position."));
    }

    @Test
    void should_return_cars_from_parkingBoy_when_fetch_given_parking_boy_and_tickets() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingBoy parkingBoy = new ParkingBoy();
        Ticket ticket = new Ticket(0);
        parkingBoy.getParkingLots().get(0).getTicketCarMap().put(ticket, new Car());

        parkingLotServiceManager.addParkingBoy(parkingBoy);

        // when
        Car car = parkingLotServiceManager.fetch(ticket);

        // then
        assertNotNull(car);
        assertEquals(0, parkingBoy.getParkingLots().get(0).getTicketCarMap().size());
    }

    @Test
    void should_throw_exception_from_parkingBoy_when_fetch_given_wrong_ticket() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingBoy parkingBoy = new ParkingBoy();

        parkingBoy.getParkingLots().clear();
        parkingBoy.getParkingLots().add(new ParkingLot(1));
        parkingLotServiceManager.addParkingBoy(parkingBoy);

        // when
        UnrecognizedPackingTicketException exception = assertThrows(UnrecognizedPackingTicketException.class,
                () -> parkingLotServiceManager.fetch(new Ticket(0)));

        // then
        assertTrue(exception.getMessage().contains("Unrecognized parking ticket."));
    }

    @Test
    void should_return_tickets_when_park_given_lot_and_car() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        Car car = new Car();

        // when
        parkingLotServiceManager.park(car, "standard");

        //then
        assertEquals(1, parkingLotServiceManager.getParkingLots().get(0).getTicketCarMap().size());
    }

    @Test
    void should_throw_exception_when_park_given_no_available_lot_and_car() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        parkingLotServiceManager.getParkingLots().clear();
        parkingLotServiceManager.getParkingLots().add(new ParkingLot(0));
        Car car = new Car();

        // when
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class,
                () -> parkingLotServiceManager.park(car, "standard"));

        // then
        assertTrue(exception.getMessage().contains("No available position."));
    }

    @Test
    void should_return_cars_when_fetch_given_tickets() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        Car car = new Car();
        Ticket ticket = new Ticket(0);
        parkingLotServiceManager.getParkingLots().get(0).getTicketCarMap().put(ticket, car);

        // when
        parkingLotServiceManager.fetch(ticket);

        // then
        assertEquals(0, parkingLotServiceManager.getParkingLots().get(0).getTicketCarMap().size());
    }

    @Test
    void should_throw_exception_when_fetch_given_wrong_tickets() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        Car car = new Car();
        Ticket ticket = new Ticket(0);
        parkingLotServiceManager.getParkingLots().get(0).getTicketCarMap().put(ticket, car);

        // when
        UnrecognizedPackingTicketException exception = assertThrows(UnrecognizedPackingTicketException.class,
                () -> parkingLotServiceManager.fetch(new Ticket(1)));

        // then
        assertTrue(exception.getMessage().contains("Unrecognized parking ticket."));
    }
}
