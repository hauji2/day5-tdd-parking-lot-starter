package com.parkinglot.strategy;

import com.parkinglot.dto.Car;
import com.parkinglot.dto.ParkingLot;
import com.parkinglot.dto.Ticket;
import com.parkinglot.exception.NoAvailablePositionException;

import java.util.List;

public class StandardParkingBoyStrategy implements RootParkingBoyStrategy {
    @Override
    public Ticket findAvailableLotToPark(Car car, List<ParkingLot> parkingLots) {
        ParkingLot availableParkingLot = parkingLots.stream().
                filter(parkingLot -> parkingLot.getTicketCarMap().size() < parkingLot.getCapacity())
                .findFirst()
                .orElseThrow(NoAvailablePositionException::new);

        return availableParkingLot.parkingCar(car);
    }
}
