package com.parkinglot.strategy;

import com.parkinglot.dto.Car;
import com.parkinglot.dto.ParkingLot;
import com.parkinglot.dto.Ticket;

import java.util.List;

public interface RootParkingBoyStrategy {
    Ticket findAvailableLotToPark(Car car, List<ParkingLot> parkingLots);
}
