package com.parkinglot.strategy;

import com.parkinglot.dto.Car;
import com.parkinglot.dto.ParkingLot;
import com.parkinglot.dto.Ticket;
import com.parkinglot.exception.NoAvailablePositionException;

import java.util.Comparator;
import java.util.List;

public class SmartParkingBoyStrategy implements RootParkingBoyStrategy {
    @Override
    public Ticket findAvailableLotToPark(Car car, List<ParkingLot> parkingLots) {
        ParkingLot availableParkingLot = parkingLots.stream()
                .max(Comparator.comparingInt(lot -> lot.getCapacity() - lot.getTicketCarMap().size()))
                .orElseThrow(NoAvailablePositionException::new);

        return availableParkingLot.parkingCar(car);
    }
}
