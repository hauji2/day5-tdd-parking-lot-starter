package com.parkinglot.dto;

import com.parkinglot.exception.UnrecognizedPackingTicketException;
import com.parkinglot.strategy.SmartParkingBoyStrategy;
import com.parkinglot.strategy.StandardParkingBoyStrategy;
import com.parkinglot.strategy.SuperParkingBoyStrategy;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class ParkingBoy {


    private List<ParkingLot> parkingLots = new ArrayList<>();

    public ParkingBoy() {
        this.parkingLots = new ArrayList<>();
        this.parkingLots.add(new ParkingLot());
    }

    public Ticket park(Car car, String parkingBoyType) {
        switch (parkingBoyType) {
            case "smart":
                SmartParkingBoyStrategy smartParkingBoyStrategy = new SmartParkingBoyStrategy();
                return smartParkingBoyStrategy.findAvailableLotToPark(car, parkingLots);
            case "super":
                SuperParkingBoyStrategy superParkingBoyStrategy = new SuperParkingBoyStrategy();
                return superParkingBoyStrategy.findAvailableLotToPark(car, parkingLots);
            default:
                StandardParkingBoyStrategy standardParkingBoyStrategy = new StandardParkingBoyStrategy();
                return standardParkingBoyStrategy.findAvailableLotToPark(car, parkingLots);
        }
    }

    public Car fetch(Ticket ticket) {
        ParkingLot matchedParkingLot = parkingLots.stream().
                filter(parkingLot -> parkingLot.getTicketCarMap().containsKey(ticket))
                .findFirst().orElseThrow(UnrecognizedPackingTicketException::new);

        return matchedParkingLot.fetchCar(ticket);
    }

    protected boolean isAvailable() {
        return parkingLots.stream()
                .anyMatch(parkingLot -> parkingLot.getTicketCarMap().size() < parkingLot.getCapacity());
    }
}
