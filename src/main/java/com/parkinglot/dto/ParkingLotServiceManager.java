package com.parkinglot.dto;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedPackingTicketException;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class ParkingLotServiceManager extends ParkingBoy {

    private List<ParkingBoy> parkingBoys = new ArrayList<>();

    public void addParkingBoy(ParkingBoy parkingBoy) {
        parkingBoys.add(parkingBoy);
    }

    public Ticket specifyParkingBoyToPark(Car car) {
        ParkingBoy parkingBoy = findAvailableParkingBoy();
        return parkingBoy.park(car, "standard");

    }

    @Override
    public Car fetch(Ticket ticket) {
        try {
            return super.fetch(ticket);
        } catch (UnrecognizedPackingTicketException e) {
            ParkingBoy targetParkingBoy = findParkingBoyWithTicket(ticket);
            return targetParkingBoy.fetch(ticket);
        }
    }

    private ParkingBoy findParkingBoyWithTicket(Ticket ticket) {
        return parkingBoys.stream()
                .filter(parkingBoy -> parkingBoy.getParkingLots().stream()
                        .anyMatch(parkingLot -> parkingLot.getTicketCarMap().containsKey(ticket)))
                .findFirst()
                .orElseThrow(UnrecognizedPackingTicketException::new);
    }

    private ParkingBoy findAvailableParkingBoy() {
        return parkingBoys.stream()
                .filter(ParkingBoy::isAvailable)
                .findFirst()
                .orElseThrow(NoAvailablePositionException::new);
    }

}
