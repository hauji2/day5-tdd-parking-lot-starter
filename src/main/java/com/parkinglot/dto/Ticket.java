package com.parkinglot.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@Data
@SuperBuilder(toBuilder = true)
public class Ticket {
    private Integer parkingLotId;
}
