package com.parkinglot.dto;

import com.parkinglot.exception.NoAvailablePositionException;
import com.parkinglot.exception.UnrecognizedPackingTicketException;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class ParkingLot {
    private Map<Ticket, Car> ticketCarMap = new HashMap<>();

    private Integer capacity;

    public ParkingLot() {
        this.capacity = 10;
    }

    public ParkingLot(Integer capacity) {
        this.capacity = capacity;
    }

    public Car fetchCar(Ticket ticket) {
        if (!ticketCarMap.containsKey(ticket)) {
            throw new UnrecognizedPackingTicketException();
        }

        Car car = ticketCarMap.get(ticket);

        ticketCarMap.remove(ticket);

        return car;
    }

    public Ticket parkingCar(Car car) {
        if (ticketCarMap.size() >= capacity) {
            throw new NoAvailablePositionException();
        }
        Ticket ticket = new Ticket(ticketCarMap.size() + 1);
        ticketCarMap.put(ticket, car);


        return ticket;
    }
}
