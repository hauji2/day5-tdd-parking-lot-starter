package com.parkinglot.exception;

public class UnrecognizedPackingTicketException extends RuntimeException {

    public UnrecognizedPackingTicketException() {
        super("Unrecognized parking ticket.");
    }
}
